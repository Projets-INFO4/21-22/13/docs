TABLEAU DE BORD PROJET GÉOLOCALISATION:

Semaine du 24/01/2022 : 
récupération du matériel util au projet
installation de RIOT OS
étude du cours sur RIOT OS (Getting started)

Semaine du 31/01/2022:

Programmation de l'ESP 32 depuis Arduino IDE
https://electropeak.com/learn/getting-started-with-the-esp32/ 
https://github.com/thingsat/tinygs_2g4station/tree/main/Firmware

Nous avons réalisé les tâches suivantes, toutes nos cartes fonctionnent bien pour l’envoie et la réception de messages à travers un partage de connexion. Nous avons eu pas mal de soucis pendant l’installation qui nous ont fait perdre du temps pendant la séance commune. 
Baptiste : problème au niveau de l’installation “ImportError : no module named serial”
malgré l’installation de ce module via pip install serial; pas de problème pour les 2 autres membres (peut-être dû à la VM ?) 

j’ai réessayé l’install pendant le weekend mais rien ne change

Semaine du 07/02/2022:
Réunion pour le groupe TinyGS (groupe de projet 2)
LoRa SX1280 (quatrième lien du doc à faire)
	pour trouver les fichiers faire : 
fichier / ouvrir / libraries / SX12XX-LoRa-master/examples/SX128x_examples/Basics/ …
Voir la partie utilitaire sur l’application

Mail de Mr Donsez 
Pour les groupes de projet (S8 et S10) qui mettent en oeuvre des « objets » LoRa:

Bonsoir,
Une gateway LoRa vient d’être installée dans un bureau de l’accueil (P204)
Le traffic qu’elle reçoit est visible ici
https://lns.campusiot.imag.fr/#/organizations/22/gateways/353036201a003200/frames
Vous pourrez voir passer les messages émis par les endpoints que vous utilisez pour vos projets.

Bonne soirée,

Baptiste : installation terminée de arduino

Semaine du 14/02/2022

Réunion : 
-Les étapes à suivre : 
-il faut s’envoyer des paquets en étant proche
-puis réaliser des tests de distances comme dans le lien du projet : https://github.com/StuartsProjects/SX1280_Testing 
-en calculant les distances : il faut faire attention à la zone de frenelle 
https://en.wikipedia.org/wiki/Fresnel_zone
	-pour aider à avoir des coordonnées GPS, on peut récupérer un GPS au fablab nommé GroovGPS : https://wiki.seeedstudio.com/Grove-GPS/  mais il existe toujours des problèmes de précision avec ce genre de GPS. Le plus simple est de se repérer sur une carte et de récupérer les coordonnées géographique plus tard.
	-pour calculer au mieux les distances lors des tests, nous avons la formule suivante : https://fr.wikipedia.org/wiki/Formule_de_haversine

Soutenance de mi-projet: 
	-cf mail Olivier Richard

Ce qu’on a fait :
	-Baptiste à fini l’installation suivante : https://github.com/thingsat/tinygs_2g4station/tree/main/Firmware/Arduino#toolchain-and-sx128x-libraries
	-Nous devons maintenant tester l’envoie et la réception à très courte distance. Une fois cela réalisé, nous mettrons en place un protocole pour tester les distances. 
	-Compte wiki (cf mail 31 janvier olivier richard) http://air.imag.fr

Semaine du 21/02/2022

Réunion pour préparer la soutenance de mi-projet :
	-mise en commun des idées
	-répartition des tâches 


Semaine du 28/02/2022

Lundi : soutenance de mi-projet
Mardi : Travail sur les cartes, 
-Réflexion sur comment nous allons faire les mesures. (utilisation du temps d’envoie du message pour faire un calcul de distance (v = d/t))
Nous avons donc le lien suivant pour récupérer le temps sur Arduino:
https://randomnerdtutorials.com/epoch-unix-time-esp32-arduino/
    -Nous allons surement nous servir de MQTT pour s’envoyer des messages qui contiennent du temps. 

Semaine du 07/03/2022

https://github.com/thingsat/tinygs_2g4station/tree/main/Firmware/Arduino#toolchain-and-sx128x-libraries
-Nous avons repris dans le lien ci-dessus les dossier directement fournis dans le Git (ces dossiers nous fournissent plus de settings ce qui nous à permis d’avoir le bon affichage dans la console)
-Nous avons remarqué que le transmitter envoie le temps qu’un parquet met pour arriver (TransmitterTime = …ms) et le receiver envoie l’heure précise à laquelle le message à été reçus (ex 20:09:03.773) 
        -Peut-on se servir de ses données pour calculer le temps ? Non 
        -Système d’aller-retours 
        -référence de temps précise : 
https://en.wikipedia.org/wiki/White_Rabbit_Project
        -RTT ?? 


-Nous avons changé de channel  dans le fichier SettingsModulation -> nous voyons toujours apparaître nos messages, et nous filtrons les messages de l’autre groupe. 

-Nous avons modifié le packet_delay toujours dans le même fichier afin d’avoir une réception plus espacée des messages.

09/03/2022
https://arduino.stackexchange.com/questions/23378/arduino-to-send-sms-one-time-only-when-a-button-is-pressed

    Nous pensons utiliser le bouton sur la carte, pour envoyer uniquement un seul message à la fois (et non pas utiliser la loop). Nous aurons alors un seul programme à lancer pour chaque carte qui jouera le rôle de transmetteur et de récepteur. Nous faisons tout cela pour implémenter un RTT (un paquet est envoyé par une carte et dès la réception par l’autre carte, nous envoyons un paquet à l'émetteur). 

15/03/2022

Nous avons créé un fichier arduino qui fusionne le code du “Transmitter” et du "Receiver". Nous pouvons désormais, sur une seule carte, avoir les deux rôles. Par défaut, c’est le rôle du transmetteur mais en appuyant sur le bouton boot de la carte, on passe en receveur (à l’aide d’un booléen défini dans notre code).
Nous avons également fait des test avec les 3 cartes en même temps et tout fonctionne comme nous le voulons. 

Reste à faire : trouver ce qu’on doit mettre dans le message, comment mettre le temps précis dedans. 
21/03/2022:
Découverte du Ranging à travers les liens suivants : 
https://digitalcollection.zhaw.ch/bitstream/11475/22769/3/2021_Mueller-etal_Outdoor-ranging-and-positioning-based-on-LoRa-modulation.pdf 

https://lora-developers.semtech.com/uploads/documents/files/TheoryAndPrinciples_AdvancedRanging_SX1280_v7.pdf

Nous avons trouvé un code d’exemple dans :
https://github.com/StuartsProjects/SX12XX-LoRa/tree/master/examples/SX128x_examples/Ranging

Nous cherchons une solution pour le faire fonctionner

28/03/2022:
Nous avons commencé le rapport final de projet, 
Nous cherchons toujours une solution pour notre code de Ranging (envoi de mail à M.Donsez)

Nous avons finalement trouvé une solution, le code se trouvait dans un git que nous avions déjà exploré.  Nous avons modifié quelques passages et ajouté des fichiers de settings pour que celui-ci puisse fonctionner.

Durant la même semaine, nous sommes également allé faire les mesures nécessaires. Nous avons tout d’abord réfléchi ensemble au protocole et à des points de mesures qui nous semblaient pertinents. Puis nous nous sommes mis en route.  

Semaine du 04/04/2022: 

Nous finalisons les idées à mettre dans le rapport et nous commençons à rédiger. Nous alimentons également le plus possible notre git, avec toutes les informations et tests que nous avons pu trouver et qui nous ont aidé pour avancer dans notre projet. 

Nous nous préparons aussi pour l’oral de la soutenance. 


	









